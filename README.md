# Description
The Weather component allows to display weather information of a given city.
It makes a GET request to the OpenWeather API to fetch the current temperature, a description of the weather
and a representative icon for the weather.

# Usage
In order to add the component to the DOM, you will need to add a city name into the city prop.

ex. `<Weather city="Montreal"/>`

# Attributions
OpenWeather API - https://openweathermap.org

Favicon by Those Icons on flaticon.com - https://www.flaticon.com/free-icon/cloudy_578116?term=weather&page=1&position=5&related_item_id=578116--
