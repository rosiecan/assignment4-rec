/**
 * @author Rosemary Canon
 */

"use strict";

/**
 * Component that shows the weather of a given city.
 */
class Weather extends React.Component {
  /**
   * Sets the state with two variables: first_render and error. first_render will be used
   * to indicate if the first render has happened. error will used in order to indiacate an error
   * has occured within the program.
   * @param {*} props contains the city name
   */
  constructor(props) {
    super(props);
    this.state = {
      first_render: true,
      error: false
    };
  }

  /**
   * This function returns the appropriate DOM component that displays the information in state. If the error in
   * state is true, an appropriate message will be displayed. If false, the city name, current weather, the description
   * of the weather and a corresponding image are displayed. The user is also presented with a button that allows
   * to "refresh" (fetch again) the information from the OpenWeather API.
   */
  render() {
    //When the first render happens, we want to call the componentDidMount
    if (this.state.first) {
      this.setState({
        first_render: false
      });

      this.componentDidMount();
    }

    if (this.state.error) {
      return (
        <div class="info">
          <p>Sorry, no information available right now</p>
          <button onClick={() => this.componentDidMount()}>Refresh</button>
        </div>
      );
    } else {
      return (
        <div class="info">
          <p>{this.props.city}</p>
          <p>
            {this.state.temperature}°C {this.state.description}
          </p>
          <img src={this.state.imgLink} alt="img" />
          <button onClick={() => this.componentDidMount()}>Refresh</button>
        </div>
      );
    }
  }

  /**
   * This function will make a fetch request to the OpenWeather API. If an error occurs, it will saved within state.
   * If the fetch is successful, we will extract certain data from the json object that is returned which will then
   * be used in the render function.
   */
  componentDidMount() {
    //This is the key that we will be using in order to use the OpenWeather API
    const APIkey = "06b70235ddccc4c58d0159cd11862ba7";
    // We will need to create an object containing the information needed to create our querystring
    const data = {
      q: this.props.city,
      appid: APIkey,
      units: "metric" // we need the temperature in Celsius
    };
    const params = new URLSearchParams(data);
    const url = "https://api.openweathermap.org/data/2.5/weather?" + params.toString();

    //GET request to the url above.
    fetch(url)
      .then(response => {
        if (!response.ok) {
          throw new Error("Error status" + response.status);
        } else {
          return response.json();
        }
      })
      //Once we retrieve the json object, we will set new variables into the state in order to use them in the render function.
      .then(json =>
        this.setState({
          error: false, //set it to false if it was previously true
          temperature: json.main.temp,
          description: json.weather[0].description,
          imgLink: "http://openweathermap.org/img/wn/" + json.weather[0].icon + "@2x.png"
        })
      )
      .catch(error => {
        console.log("An error has occurred: " + error);
        //if theres an error while doing the fetch set the error to true
        this.setState({
          error: true
        });
      });
  }
}

// Add component to div #root.
ReactDOM.render(<Weather city="Montreal" />, document.querySelector("#root"));
